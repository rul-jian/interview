package com.bibao.interview.q001;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Peter Xiong on 9/15/2019
 */
public class Fibonacci {
	protected static final List<Long> FIBO_VALUES = new ArrayList<>();
	
	static {
		FIBO_VALUES.add(0L);
		FIBO_VALUES.add(1L);
		while (true) {
			int n = FIBO_VALUES.size();
			long x = FIBO_VALUES.get(n-1) + FIBO_VALUES.get(n-2);
			if (x<0) break;
			FIBO_VALUES.add(x);
		}
	}
	
	public long getRecursive(int n) {
		if (n<0) throw new InvalidInputException("Input number cannot be negative");
		if (n==0) return 0L;
		if (n==1) return 1L;
		return getRecursive(n-1) + getRecursive(n-2);
	}
	
	public long getNonRecursive(int n) {
		if (n<0) throw new InvalidInputException("Error: Input number cannot be negative");
		if (n==0) return 0L;
		if (n==1) return 1L;
		long a = 0;
		long b = 1;
		for (int i=2; i<=n; i++) {
			b = a + b;
			a = b - a;
			// Use one more temp variable
//			long c = a + b;
//			a = b;
//			b = c;
		}
		return b;
	}
	
	/**
	 * Scenario #1: n-step stairs with going up by 1-step or 2-step walk, how many different ways?
	 * Scenario #2: There are length-1 brick and length-2 brick, how many different ways to build 
	 * 				a brick wall with length n?
	 */
	public long getByMath(int n) {
		if (n<0) throw new InvalidInputException("Error: Input number cannot be negative");
		int m = n/2;
		long r = 0;
		for (int i=0; i<=m; i++) {
			r = r + combinatory(n - i, i);
		}
		return r;
	}
	
	private long combinatory(int n, int k) {
		long c = 1;
		if (k>n/2) k = n - k;
		for (int m=n, j=1; m>=n-k+1; m--, j++) {
			c = c * m / j;
		}
		return c;
	}
}
