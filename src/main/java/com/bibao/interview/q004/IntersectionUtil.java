package com.bibao.interview.q004;

/**
 * @author Peter Xiong on 9/22/2019
 */
public class IntersectionUtil {
	/************************************************************
	 * Description: Find intersections of lines or rectangles	*
	 ************************************************************/
	
	public boolean isIntersect(Interval v1, Interval v2) {
		return !isDisjoint(v1, v2);
	}
	
	public boolean isDisjoint(Interval v1, Interval v2) {
		return v1.getY()<v2.getX() || v2.getY()< v1.getX();
	}
	
	public int getIntersectArea(Interval v1, Interval v2) {
		if (isDisjoint(v1, v2)) return 0;
		return Math.min(v1.getY(), v2.getY()) - Math.max(v1.getX(), v2.getX());
	}
	
	public boolean isIntersect(Rectangle r1, Rectangle r2) {
		return !isDisjoint(r1, r2);
	}
	
	public boolean isDisjoint(Rectangle r1, Rectangle r2) {
		return r1.getUpperLeftX()>r2.getBottomRightX() || r1.getUpperLeftY()>r2.getBottomRightY()
				|| r2.getUpperLeftX()>r1.getBottomRightX() || r2.getUpperLeftY()>r1.getBottomRightY();
	}
	
	public int getIntersectArea(Rectangle r1, Rectangle r2) {
		if (isDisjoint(r1, r2)) return 0;
		int leftUpperX = Math.max(r1.getUpperLeftX(), r2.getUpperLeftX());
		int leftUpperY = Math.max(r1.getUpperLeftY(), r2.getUpperLeftY());
		int bottomRightX = Math.min(r1.getBottomRightX(), r2.getBottomRightX());
		int bottomRightY = Math.min(r1.getBottomRightY(), r2.getBottomRightY());
		return (bottomRightY - leftUpperY) * (bottomRightX - leftUpperX);
	}
}
