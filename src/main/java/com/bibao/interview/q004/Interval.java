package com.bibao.interview.q004;

/**
 * @author Peter Xiong on 9/22/2019
 */
public class Interval {
	private int x;
	private int y;
	
	public Interval() {}
	
	public Interval(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
}
