package com.bibao.interview.q004;

/**
 * @author Peter Xiong on 9/22/2019
 */
public class Rectangle {
	private int upperLeftX;
	private int upperLeftY;
	private int bottomRightX;
	private int bottomRightY;
	
	public Rectangle() {}
	public Rectangle(int x1, int y1, int x2, int y2) {
		this.upperLeftX = x1;
		this.upperLeftY = y1;
		this.bottomRightX = x2;
		this.bottomRightY = y2;
	}
	
	public int getUpperLeftX() {
		return upperLeftX;
	}
	public void setUpperLeftX(int upperLeftX) {
		this.upperLeftX = upperLeftX;
	}
	public int getUpperLeftY() {
		return upperLeftY;
	}
	public void setUpperLeftY(int upperLeftY) {
		this.upperLeftY = upperLeftY;
	}
	public int getBottomRightX() {
		return bottomRightX;
	}
	public void setBottomRightX(int bottomRightX) {
		this.bottomRightX = bottomRightX;
	}
	public int getBottomRightY() {
		return bottomRightY;
	}
	public void setBottomRightY(int bottomRightY) {
		this.bottomRightY = bottomRightY;
	}
}
