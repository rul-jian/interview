package com.bibao.interview.q006;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

/**
 * @author Peter Xiong on 9/24/2019
 */
public class BST {
	/************************************************************
	 * Description: Use non-recursive way to traverse a 		*
	 * 				binary pre-order, in-order or post-order	*
	 ************************************************************/
	
	private Node root;
	
	public void add(int value) {
		if (root==null) {
			root = new Node();
			root.value = value;
			return;
		}
		Node current = root;
		Node parent = current;
		while (current!=null) {
			if (value<current.value) {
				parent = current;
				current = current.leftChild;
			} else {
				parent = current;
				current = current.rightChild;
			}
		}
		Node node = new Node();
		node.value = value;
		if (node.value<parent.value) parent.leftChild = node;
		else parent.rightChild = node;
	}
	
	public List<Integer> traverseByLevel() {
		List<Integer> result = new ArrayList<>();
		if (root==null) return result;
		Queue<Node> queue = new LinkedList<>();
		queue.add(root);
		while (!queue.isEmpty()) {
			Node current = queue.poll();
			result.add(current.value);
			if (current.leftChild!=null) queue.add(current.leftChild);
			if (current.rightChild!=null) queue.add(current.rightChild);
		}
		
		return result;
	}
	
	/****************************
	 * 		Pre-Order Traverse	*
	 ****************************/
	
	public List<Integer> preOrderTraverse() {
		List<Integer> result = new ArrayList<>();
		preOrderRecursive(root, result);
		return result;
	}
	
	private void preOrderRecursive(Node node, List<Integer> result) {
		if (node==null) return;
		result.add(node.value);
		preOrderRecursive(node.leftChild, result);
		preOrderRecursive(node.rightChild, result);
	}
	
	public List<Integer> preOrderTraverseNonRecursive() {
		List<Integer> result = new ArrayList<>();
		Stack<Node> stack = new Stack<>();
		stack.add(root);
		while (!stack.isEmpty()) {
			Node current = stack.pop();
			result.add(current.value);
			if (current.rightChild!=null) stack.add(current.rightChild);
			if (current.leftChild!=null) stack.add(current.leftChild);
		}
		return result;
	}
	
	/****************************
	 * 		In-Order Traverse	*
	 ****************************/
	public List<Integer> inOrderTraverse() {
		List<Integer> result = new ArrayList<>();
		inOrderRecursive(root, result);
		return result;
	}
	
	private void inOrderRecursive(Node node, List<Integer> result) {
		if (node==null) return;
		inOrderRecursive(node.leftChild, result);
		result.add(node.value);
		inOrderRecursive(node.rightChild, result);
	}
	
	public List<Integer> inOrderTraverseNonRecursive() {
		List<Integer> result = new ArrayList<>();
		Stack<Node> stack = new Stack<>();
		stack.add(root);
		root.visited = true;
		while (!stack.isEmpty()) {
			Node current = stack.lastElement();
			if (current.leftChild!=null && !current.leftChild.visited) {
				stack.add(current.leftChild);
				current.leftChild.visited = true;
				continue;
			}
			current = stack.pop();
			result.add(current.value);
			if (current.rightChild!=null) stack.add(current.rightChild);
		}
		return result;
	}
	
	/****************************
	 * 		Post-Order Traverse	*
	 ****************************/
	public List<Integer> postOrderTraverse() {
		List<Integer> result = new ArrayList<>();
		postOrderRecursive(root, result);
		return result;
	}
	
	private void postOrderRecursive(Node node, List<Integer> result) {
		if (node==null) return;
		postOrderRecursive(node.leftChild, result);
		postOrderRecursive(node.rightChild, result);
		result.add(node.value);
	}
	
	public List<Integer> postOrderTraverseNonRecursive() {
		List<Integer> result = new ArrayList<>();
		Stack<Node> stack = new Stack<>();
		stack.add(root);
		root.visited = true;
		while (!stack.isEmpty()) {
			Node current = stack.lastElement();
			if ((current.leftChild==null || current.leftChild.visited) 
					&& (current.rightChild==null || current.rightChild.visited)) {
				current = stack.pop();
				result.add(current.value);
				continue;
			}
			if (current.rightChild!=null && !current.rightChild.visited) {
				stack.add(current.rightChild);
				current.rightChild.visited = true;
			}
			if (current.leftChild!=null && !current.leftChild.visited) {
				stack.add(current.leftChild);
				current.leftChild.visited = true;
			}
		}
		return result;
	}
	
	class Node {
		int value;
		boolean visited;
		Node leftChild;
		Node rightChild;
	}
}
