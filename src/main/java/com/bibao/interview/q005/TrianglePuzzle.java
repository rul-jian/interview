package com.bibao.interview.q005;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * @author Peter Xiong on 9/22/2019
 */
public class TrianglePuzzle {
	private static final Logger LOG = Logger.getLogger(TrianglePuzzle.class);
			
	private int[] data;
	private Node[] nodes;

	public TrianglePuzzle() {}
	
	public TrianglePuzzle(int[] data) {
		this.data = data;
		init();
		displayNodes();
	}
	
	private void init() {
		int n = data.length;
		nodes = new Node[n];
		int i=0;
		int level = 0;
		while (i<n) {
			for (int j=0; j<=level; j++) {
				Node node = new Node();
				node.value = data[i];
				node.totalValue = 0;
				node.index = i;
				node.parentIndex = -1;
				node.level = level;
				nodes[i] = node;
				i++;
			}
			level++;
		}
		nodes[0].totalValue = nodes[0].value;
	}
	
	public int solve() {
		int sum = nodes[0].totalValue;
		int index = 0;
		int n = nodes.length;
		for (int i=0; i<n; i++) {
			int leftIndex = nodes[i].leftChild();
			int rightIndex = nodes[i].rightChild();
			if (leftIndex>=n || rightIndex>=n) continue;
			int leftTotalValue = nodes[i].totalValue + nodes[leftIndex].value;
			if (leftTotalValue>nodes[leftIndex].totalValue) {
				nodes[leftIndex].totalValue = leftTotalValue;
				nodes[leftIndex].parentIndex = i;
				if (leftTotalValue>sum) {
					sum = leftTotalValue;
					index = leftIndex;
				}
			}
			int rightTotalValue = nodes[i].totalValue + nodes[rightIndex].value;
			if (rightTotalValue>nodes[rightIndex].totalValue) {
				nodes[rightIndex].totalValue = rightTotalValue;
				nodes[rightIndex].parentIndex = i;
				if (rightTotalValue>sum) {
					sum = rightTotalValue;
					index = rightIndex;
				}
			}
		}
		// Print the path
		List<Integer> list = new ArrayList<>();
		int currentIndex = index;
		while (currentIndex>=0) {
			list.add(nodes[currentIndex].value);
			currentIndex = nodes[currentIndex].parentIndex;
		}
		Collections.reverse(list);
		StringBuilder builder = new StringBuilder("Path: " + list.get(0));
		for (int i=1; i<list.size(); i++) builder.append(" --> " + list.get(i));
		LOG.debug(builder.toString());
		LOG.debug("Sum: " + sum);
		return sum;
	}
	
	public int[] getData() {
		return data;
	}

	public void setData(int[] data) {
		this.data = data;
	}
	
	public void displayNodes() {
		int level = 0;
		StringBuilder builder = new StringBuilder("\n");
		for (int i=0; i<nodes.length; i++) {
			if (nodes[i].level>level) {
				level = nodes[i].level;
				builder.append("\n");
			}
			builder.append(nodes[i].value + " ");
		}
		LOG.debug(builder.toString());
	}
	
	class Node {
		int value;
		int totalValue;
		int index;
		int level;
		int parentIndex;
		
		int leftChild() {
			return index + level + 1;
		}
		
		int rightChild() {
			return index + level + 2;
		}
	}
}
