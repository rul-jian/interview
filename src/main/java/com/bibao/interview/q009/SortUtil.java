package com.bibao.interview.q009;

import java.util.Arrays;

/**
 * @author Peter Xiong on 9/26/2019
 */
public class SortUtil {
	/************************************************************
	 * Description: Identify whether an array of integers		*
	 * 				can be sorted by at most one swap			*
	 ************************************************************/
	
	public boolean sortByOneSwapSimple(int... array) {
		int[] sortedArray = array.clone();
		Arrays.sort(sortedArray);				// Running time: O(nlgn)
		int countOfDiff = 0;
		for (int i=0; i<array.length; i++) {
			if (array[i]!=sortedArray[i]) countOfDiff++;
		}
		if (countOfDiff>2) return false;
		else return true;
	}
	
	public boolean sortByOneSwap(int... array) {	// Running time: O(n)
		int n = array.length;
		if (n<=2) return true;
		int[] workingArray = array.clone();
		int firstIndex = -1;
		int secondIndex = -1;
		int count = 0;
		for (int i=0; i<n-1; i++) {
			if (workingArray[i]>workingArray[i+1]) {
				count++;
				if (count==1) {
					firstIndex = i;
				} else if (count==2) {
					secondIndex = i;
				}
			}
		}
		if (count>2) return false;
		if (count==0) return true;
		int swappedIndex = -1;
		if (count==2) {
			swappedIndex = secondIndex + 1;
		} else {	// It is more complicated when count = 1
			swappedIndex = firstIndex + 1;
			while (swappedIndex<n-1 && workingArray[swappedIndex]==workingArray[swappedIndex+1]) swappedIndex++;
		}
		while (firstIndex>0 && workingArray[firstIndex]==workingArray[firstIndex-1]) firstIndex--;
		// swap
		int temp = workingArray[firstIndex];
		workingArray[firstIndex] = workingArray[swappedIndex];
		workingArray[swappedIndex] = temp;
		return isSorted(workingArray);
	}
	
	private boolean isSorted(int[] array) {
		for (int i=0; i<array.length-1; i++) {
			if (array[i]>array[i+1]) return false;
		}
		return true;
	}
}
