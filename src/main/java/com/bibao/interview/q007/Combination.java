package com.bibao.interview.q007;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Peter Xiong on 9/25/2019
 */
public class Combination {
	/********************************************************************
	 * Description: Find all combinations in a multi-dimensional space	*
	 ********************************************************************/
	
	private int[] base;
	
	public Combination(int... base) {
		this.base = base;
	}
	
	public static int reverseInt(int value) {
		int currentValue = value;
		int result = 0;
		while (currentValue>0) {
			result = result * 10 + (currentValue%10);
			currentValue = currentValue / 10;
		}
		return result;
	}
	
	public Set<String> getAllCombinations() {
		Set<String> set = new HashSet<>();
		int total = 1;
		int n = base.length;
		for (int i=0; i<n; i++) total = total * (base[i] + 1);
		for (int m=0; m<total; m++) {
			int[] result = new int[n];
			int currentValue = m;
			for (int i=0; i<n; i++) {
				result[i] = currentValue % (base[i] + 1);
				currentValue = currentValue / (base[i] + 1);
			}
			set.add(build(result));
		}
		return set;
	}
	
	private String build(int[] array) {
		StringBuilder builder = new StringBuilder("(");
		builder.append(StringUtils.join(ArrayUtils.toObject(array), ", "));
		builder.append(")");
		return builder.toString();
	}
}
