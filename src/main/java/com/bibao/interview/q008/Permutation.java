package com.bibao.interview.q008;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

/**
 * @author Peter Xiong on 9/25/2019
 */
public class Permutation {
	/************************************************************
	 * Description: Find all permuations of an input string		*
	 ************************************************************/
	
	public Set<String> permuteRecursive(String input) {
		Set<String> result = new HashSet<>();
		permute("", input, result);
		return result;
	}
	
	private void permute(String current, String remaining, Set<String> result) {
		if (StringUtils.isEmpty(remaining)) {
			result.add(current);
			return;
		}
		for (int i=0; i<remaining.length(); i++) {
			char c = remaining.charAt(i);
			String newRemaining = remaining.substring(0, i) + remaining.substring(i + 1);
			permute(current + c, newRemaining, result);
		}
	}
	
	public Set<String> permuteNonRecursive(String input) {
		Set<String> result = new HashSet<>();
		for (int i=0; i<input.length(); i++) {
			result = add(result, input.charAt(i));
		}
		return result;
	}
	
	private Set<String> add(Set<String> current, char c) {
		Set<String> afterAdd = new HashSet<>();
		if (current.size()==0) {
			afterAdd.add("" + c);
			return afterAdd;
		}
		for (String str: current) {
			for (int i=0; i<str.length(); i++) {
				afterAdd.add(str.substring(0, i) + c + str.substring(i));
			}
			afterAdd.add(str + c);
		}
		return afterAdd;
	}
}
