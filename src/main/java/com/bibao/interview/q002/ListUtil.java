package com.bibao.interview.q002;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;

/**
 * @author Peter Xiong on 9/16/2019
 */
public class ListUtil {
	
	public Integer getMax(List<Integer> list) {
		if (list==null) throw new NoElementException("Error: No element");
		List<Integer> refinedList = list.stream().filter(Objects::nonNull).collect(Collectors.toList());
		if (CollectionUtils.isEmpty(refinedList)) throw new NoElementException("Error: No element");
		Optional<Integer> result = refinedList.stream().max(Integer::compareTo);
		return result.isPresent()? result.get() : null;
	}
}
