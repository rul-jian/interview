package com.bibao.interview.q010;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.Random;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Peter Xiong on 9/28/2019
 */
public class ArrayUtilTest {
	private static final Logger LOG = Logger.getLogger(ArrayUtilTest.class);
	private static final Random R = new Random();
	
	private static final int DATA_SIZE = 100000;
	private static final int DATA_RANGE = 20000;
	
	private ArrayUtil util;
	
	@Before
	public void setUp() {
		util = new ArrayUtil();
	}

	@Test
	public void testMaxSumOfSubArrayInBrutalForce() {
		int maxSum = util.maxSumOfSubArrayInBrutalForce(-5, -8, -1, -9, -15);
		assertEquals(-1, maxSum);
		maxSum = util.maxSumOfSubArrayInBrutalForce(-5, 7, 9, -4, 5, -30, 5, -2, 10, 18, -100);
		assertEquals(31, maxSum);
		maxSum = util.maxSumOfSubArrayInBrutalForce(-5, 7, 9, -4, 5, -2, 10, 18, -100);
		assertEquals(43, maxSum);
	}

	@Test
	public void testMaxSumOfSubArray() {
		int maxSum = util.maxSumOfSubArray(-5, -8, -1, -9, -15);
		assertEquals(-1, maxSum);
		maxSum = util.maxSumOfSubArray(-5, 7, 9, -4, 5, -30, 5, -2, 10, 18, -100);
		assertEquals(31, maxSum);
		maxSum = util.maxSumOfSubArray(-5, 7, 9, -4, 5, -2, 10, 18, -100);
		assertEquals(43, maxSum);
	}
	
	@Test
	public void testRunningTime() {
		int[] data = createData();
		
		Date start = new Date();
		int maxSum1 = util.maxSumOfSubArrayInBrutalForce(data);
		Date end = new Date();
		LOG.debug("Running time for brutal force algorithm: " + (end.getTime() - start.getTime()));
		start = new Date();
		int maxSum2 = util.maxSumOfSubArray(data);
		end = new Date();
		LOG.debug("Running time for efficient algorithm: " + (end.getTime() - start.getTime()));
		assertEquals(maxSum1, maxSum2);
	}
	
	private int[] createData() {
		int[] data = new int[DATA_SIZE];
		for (int i=0; i<data.length; i++) data[i] = R.nextInt(DATA_RANGE) - DATA_RANGE / 2;
		return data;
	}
}
