package com.bibao.interview.q008;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

/**
 * @author Peter Xiong on 9/25/2019
 */
public class PermutationTest {

	@Test
	public void testPermuteRecursive() {
		Permutation p = new Permutation();
		Set<String> result = p.permuteRecursive("abcc");
		assertEquals(12, result.size());
		assertTrue(equalSet(expectedResult(), result));
	}
	
	@Test
	public void testPermuteNonRecursive() {
		Permutation p = new Permutation();
		Set<String> result = p.permuteNonRecursive("abcc");
		assertEquals(12, result.size());
		assertTrue(equalSet(expectedResult(), result));
	}

	private Set<String> expectedResult() {
		String[] result = {
				"abcc", "acbc", "accb",
				"bacc", "bcac", "bcca",
				"cabc", "cacb", "cbac",
				"cbca", "ccab", "ccba"
		};
		return new HashSet<>(Arrays.asList(result));
	}
	
	private boolean equalSet(Set<String> set1, Set<String> set2) {
		if (set1.size() != set2.size()) return false;
		for (String value: set1) {
			if (!set2.contains(value)) return false;
		}
		return true;
	}
}
