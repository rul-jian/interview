package com.bibao.interview.q009;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Date;
import java.util.Random;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Peter Xiong on 9/26/2019
 */
public class SortUtilTest {
	private static final Logger LOG = Logger.getLogger(SortUtilTest.class);
	private static final Random R = new Random();
	
	private static final int DATA_SIZE = 500000;
	private static final int DATA_RANGE = 1000000;
	
	private SortUtil util;
	
	@Before
	public void setUp() {
		util = new SortUtil();
	}

	@Test
	public void testSortByOneSwapSimple() {
		assertTrue(util.sortByOneSwapSimple(1, 2, 3, 4, 5));		// already sorted
		assertTrue(util.sortByOneSwapSimple(1, 2, 8, 5));
		assertTrue(util.sortByOneSwapSimple(1, 2, 8, 6, 5));
		assertTrue(util.sortByOneSwapSimple(1, 2, 8, 4, 5, 3));
		assertTrue(util.sortByOneSwapSimple(1, 2, 5, 5, 5, 5, 4));
		assertTrue(util.sortByOneSwapSimple(1, 2, 5, 5, 5, 5, 4, 6));
		assertTrue(util.sortByOneSwapSimple(1, 2, 5, 4, 4, 4, 4));
		assertTrue(util.sortByOneSwapSimple(1, 2, 5, 4, 4, 4, 4, 3));
		assertTrue(util.sortByOneSwapSimple(8, 5, 6, 7, 3));
		assertFalse(util.sortByOneSwapSimple(1, 2, 8, 5, 6));
		assertFalse(util.sortByOneSwapSimple(1, 7, 8, 6, 5));
		assertFalse(util.sortByOneSwapSimple(1, 2, 8, 6, 5, 4));
		assertFalse(util.sortByOneSwapSimple(1, 2, 8, 4, 5, 3, 6));
		assertFalse(util.sortByOneSwapSimple(1, 2, 5, 5, 5, 5, 4, 3));
		assertFalse(util.sortByOneSwapSimple(1, 3, 5, 4, 4, 4, 4, 2));
		assertFalse(util.sortByOneSwapSimple(1, 2, 5, 5, 4, 4));
		assertFalse(util.sortByOneSwapSimple(8, 2, 6, 7, 3));
	}

	@Test
	public void testSortedByOneSwap() {
		assertTrue(util.sortByOneSwap(1, 2, 3, 4, 5));		
		assertTrue(util.sortByOneSwap(1, 2, 8, 5));
		assertTrue(util.sortByOneSwap(1, 2, 8, 6, 5));
		assertTrue(util.sortByOneSwap(1, 2, 8, 4, 5, 3));
		assertTrue(util.sortByOneSwap(1, 2, 5, 5, 5, 5, 4));
		assertTrue(util.sortByOneSwap(1, 2, 5, 5, 5, 5, 4, 6));
		assertTrue(util.sortByOneSwap(1, 2, 5, 4, 4, 4, 4));
		assertTrue(util.sortByOneSwap(1, 2, 5, 4, 4, 4, 4, 3));
		assertTrue(util.sortByOneSwap(8, 5, 6, 7, 3));
		assertFalse(util.sortByOneSwap(1, 2, 8, 5, 6));
		assertFalse(util.sortByOneSwap(1, 7, 8, 6, 5));
		assertFalse(util.sortByOneSwap(1, 2, 8, 6, 5, 4));
		assertFalse(util.sortByOneSwap(1, 2, 8, 4, 5, 3, 6));
		assertFalse(util.sortByOneSwap(1, 2, 5, 5, 5, 5, 4, 3));
		assertFalse(util.sortByOneSwap(1, 3, 5, 4, 4, 4, 4, 2));
		assertFalse(util.sortByOneSwap(1, 2, 5, 5, 4, 4));
		assertFalse(util.sortByOneSwap(8, 2, 6, 7, 3));
	}
	
	@Test
	public void testRunningTime() {
		int[] data = createData();
		
		// Test sortByOneSwap
		int[] data1 = data.clone();
		Date start = new Date();
		assertTrue(util.sortByOneSwap(data1));
		Date end = new Date();
		LOG.debug("Running time for sortByOneSwap: " + (end.getTime() - start.getTime()));
		
		// Test sortByOneSwapSimple
		int[] data2 = data.clone();
		start = new Date();
		assertTrue(util.sortByOneSwapSimple(data2));
		end = new Date();
		LOG.debug("Running time for sortByOneSwapSimple: " + (end.getTime() - start.getTime()));
	}
	
	private int[] createData() {
		int[] data = new int[DATA_SIZE];
		for (int i=0; i<data.length; i++) data[i] = R.nextInt(DATA_RANGE);
		Arrays.sort(data);
		// Make one swap
		int first = R.nextInt(DATA_SIZE);
		int second = R.nextInt(DATA_SIZE);
		int temp = data[first];
		data[first] = data[second];
		data[second] = temp;
		return data;
	}
}
