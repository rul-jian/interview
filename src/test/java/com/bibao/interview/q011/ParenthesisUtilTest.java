package com.bibao.interview.q011;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Peter Xiong on 10/04/2019
 */
public class ParenthesisUtilTest {
	private ParenthesisUtil util;
	
	@Before
	public void setUp() throws Exception {
		util = new ParenthesisUtil();
	}

	@Test
	public void testFindAllParens() {
		assertTrue(equalSet(getExpectedResult(1), util.findAllParens(1)));
		assertTrue(equalSet(getExpectedResult(2), util.findAllParens(2)));
		assertTrue(equalSet(getExpectedResult(3), util.findAllParens(3)));
		assertTrue(equalSet(getExpectedResult(4), util.findAllParens(4)));
		// f(n) = C(2n, n) / (n + 1)
		Set<String> result = util.findAllParens(5);
		assertEquals(getParensSize(5), result.size());
		result = util.findAllParens(10);			
		assertEquals(getParensSize(10), result.size());
	}

	@Test
	public void testFindAllParensNonRecursive() {
		assertTrue(equalSet(getExpectedResult(1), util.findAllParensNonRecursive(1)));
		assertTrue(equalSet(getExpectedResult(2), util.findAllParensNonRecursive(2)));
		assertTrue(equalSet(getExpectedResult(3), util.findAllParensNonRecursive(3)));
		assertTrue(equalSet(getExpectedResult(4), util.findAllParensNonRecursive(4)));
		Set<String> result = util.findAllParensNonRecursive(5);
		assertEquals(getParensSize(5), result.size());
		result = util.findAllParensNonRecursive(10);			
		assertEquals(getParensSize(10), result.size());
	}
	
	private boolean equalSet(Set<String> set1, Set<String> set2) {
		if (set1.size() != set2.size()) return false;
		for (String value: set1) {
			if (!set2.contains(value)) return false;
		}
		return true;
	}
	
	private Set<String> getExpectedResult(int n) {
		Set<String> result = new HashSet<>();
		switch(n) {
		case 1: result.addAll(Arrays.asList("()")); break;
		case 2: result.addAll(Arrays.asList("(())", "()()")); break;
		case 3: 
			result.addAll(Arrays.asList(
					/*"(())",*/ "()(())", "(())()", "((()))",
					/*"()()",*/ "()()()", "(()())"
					)); 
			break;
		case 4:
			result.addAll(Arrays.asList(
					/*"((()))",*/ "()((()))", "((()))()", "(((())))",
					/*"(()())",*/ "()(()())", "(()())()", "((()()))",
					/*"()()()",*/ "()()()()", "(()()())", "(())(())",
					/*"()(())",*/ "()(())()", "()()(())", "(()(()))",
					/*"(())()",*/ "(())()()", "()(())()", "((())())"
					)); 
			break;
		}
		return result;
	}
	
	private long combinatory(int n, int k) {
		long c = 1;
		if (k>n/2) k = n - k;
		for (int m=n, j=1; m>=n-k+1; m--, j++) {
			c = c * m / j;
		}
		return c;
	}
	
	private int getParensSize(int n) {
		return (int)(combinatory(2*n, n) / (n + 1));
	}
}
