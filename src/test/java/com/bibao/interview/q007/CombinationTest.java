package com.bibao.interview.q007;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * @author Peter Xiong on 9/25/2019
 */
public class CombinationTest {
	private static final Logger LOG = Logger.getLogger(CombinationTest.class);
	
	@Test
	public void testReverseInt() {
		assertEquals(357, Combination.reverseInt(753));
		assertEquals(8, Combination.reverseInt(8));
		assertEquals(654321, Combination.reverseInt(123456));
	}
	
	// Find all integer points in a cube from (0, 0, 0) to (1, 1, 2)
	@Test
	public void testIntPointsInCube() {
		Combination combination = new Combination(1, 1, 2);
		Set<String> actualResult = combination.getAllCombinations();
		assertTrue(equalSet(expectedResult(), actualResult));
	}

	private Set<String> expectedResult() {
		List<String> list = Arrays.asList(
				"(0, 0, 0)", "(0, 0, 1)", "(0, 0, 2)", 
				"(0, 1, 0)", "(0, 1, 1)", "(0, 1, 2)", 
				"(1, 0, 0)", "(1, 0, 1)", "(1, 0, 2)", 
				"(1, 1, 0)", "(1, 1, 1)", "(1, 1, 2)"
		);
		return new HashSet<>(list);
	}
	
	private boolean equalSet(Set<String> set1, Set<String> set2) {
		if (set1.size() != set2.size()) return false;
		for (String value: set1) {
			if (!set2.contains(value)) return false;
		}
		return true;
	}
	
	// Generic case
	@Test
	public void testGetAllCombination() {
		Combination combination = new Combination(2, 3, 5, 2);
		Set<String> result = combination.getAllCombinations();
		assertEquals(216, result.size());
		logResult(result);
		combination = new Combination(9, 9);
		result = combination.getAllCombinations();
		assertEquals(100, result.size());
		logResult(result);
	}
	
	private void logResult(Set<String> result) {
		List<String> list = new ArrayList<>(result);
		Collections.sort(list);
		LOG.debug(StringUtils.join(list, ","));
	}
}
