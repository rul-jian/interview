package com.bibao.interview.q004;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Peter Xiong on 9/22/2019
 */
public class IntersectionUtilTest {
	private IntersectionUtil util;
	
	@Before
	public void setUp() {
		util = new IntersectionUtil();
	}

	@Test
	public void testIsIntersectForInterval() {
		assertTrue(util.isIntersect(new Interval(3, 8), new Interval(5, 9)));
		assertTrue(util.isIntersect(new Interval(3, 8), new Interval(3, 6)));
		assertTrue(util.isIntersect(new Interval(3, 8), new Interval(8, 9)));
		assertTrue(util.isIntersect(new Interval(5, 9), new Interval(2, 5)));
		assertTrue(util.isIntersect(new Interval(5, 9), new Interval(2, 10)));
		assertFalse(util.isIntersect(new Interval(5, 9), new Interval(2, 3)));
		assertFalse(util.isIntersect(new Interval(3, 8), new Interval(9, 12)));
	}

	@Test
	public void testGetIntersectAreaForInterval() {
		assertEquals(3, util.getIntersectArea(new Interval(3, 8), new Interval(5, 9)));
		assertEquals(3, util.getIntersectArea(new Interval(3, 8), new Interval(3, 6)));
		assertEquals(0, util.getIntersectArea(new Interval(3, 8), new Interval(8, 9)));
		assertEquals(0, util.getIntersectArea(new Interval(5, 9), new Interval(2, 5)));
		assertEquals(4, util.getIntersectArea(new Interval(5, 9), new Interval(2, 10)));
		assertEquals(0, util.getIntersectArea(new Interval(5, 9), new Interval(2, 3)));
		assertEquals(0, util.getIntersectArea(new Interval(3, 8), new Interval(9, 12)));
	}
	
	@Test
	public void testIsIntersectForRectangle() {
		assertTrue(util.isIntersect(new Rectangle(2, 2, 8, 5), new Rectangle(3, 3, 7, 6)));
		assertTrue(util.isIntersect(new Rectangle(2, 2, 8, 5), new Rectangle(3, 1, 5, 8)));
		assertFalse(util.isIntersect(new Rectangle(2, 2, 8, 5), new Rectangle(9, 4, 10, 6)));
		assertFalse(util.isIntersect(new Rectangle(2, 2, 8, 5), new Rectangle(5, 6, 10, 8)));
	}
	
	@Test
	public void testGetIntersectAreaForRectangle() {
		assertEquals(8, util.getIntersectArea(new Rectangle(2, 2, 8, 5), new Rectangle(3, 3, 7, 6)));
		assertEquals(6, util.getIntersectArea(new Rectangle(2, 2, 8, 5), new Rectangle(3, 1, 5, 8)));
		assertEquals(0, util.getIntersectArea(new Rectangle(2, 2, 8, 5), new Rectangle(9, 4, 10, 6)));
		assertEquals(0, util.getIntersectArea(new Rectangle(2, 2, 8, 5), new Rectangle(5, 6, 10, 8)));
	}
}
